<?php

namespace file\components;

use file\models\File;
use Yii;

class CdnUrlHelper
{
    public static function getCdnUrl(File $file): string
    {
        return getenv('CDN_URL') . "/" . Yii::$app->modules['file']->getSubDirs($file->hash) . DIRECTORY_SEPARATOR . $file->hash . '.' . $file->type;
    }
}