<?php

use yii\db\Migration;
use yii\db\Schema;

class m150127_040544_files extends Migration
{
    use \file\FileModuleTrait;

    //    public $tableOptions = 'PARTITION BY LIST ("model")';
    public $tableOptions;

    public function up()
    {
        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'model' => $this->string(255)->notNull(),
            'itemId' => $this->integer(11)->notNull(),
            'hash' => $this->string(255)->notNull(),
            'size' => $this->bigInteger(20)->unsigned()->null(),
            'type' => $this->string(255)->notNull(),
            'mime' => $this->string(255)->notNull(),
            'is_main' => $this->boolean()->defaultValue(false),
            'date_upload' => $this->integer(11)->null(),
            'sort' => $this->integer(11)->notNull()->defaultValue(1),
            'type_file_id' => $this->integer()->comment('File type ID'),
            'moderated' => $this->boolean()->defaultValue(false)->comment('File is moderated')
        ], $this->tableOptions);

        $this->createIndex('file_item_id', $this->getModule()->tableName, 'itemId');
    }

    public function down()
    {
        $this->dropTable('file');
    }
}
