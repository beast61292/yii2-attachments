<?php

namespace file\models;

/**
 * This is the ActiveQuery class for [[\app\common\models\File]].
 *
 * @see \app\common\models\File
 */
class FileQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['moderated' => true]);
    }

    /**
     * {@inheritdoc}
     * @return \app\common\models\File[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\common\models\File|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
