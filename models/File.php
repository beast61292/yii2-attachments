<?php

namespace file\models;

use file\FileModuleTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $name
 * @property string $model
 * @property integer $itemId
 * @property string $hash
 * @property integer $size
 * @property string $type
 * @property string $mime
 * @property integer $is_main
 * @property integer $date_upload
 * @property integer $sort
 */
class File extends ActiveRecord
{
    use FileModuleTrait;

    const MAIN = 1;
    const NOT_MAIN = 0;

    const FILE_IMG = 1;
    const FILE_ZIP = 2;
    const FILE_IMG_ZIP = 3;
    const FILE_RATING = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return \Yii::$app->getModule('file')->tableName;
    }

    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_upload',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'model', 'itemId', 'hash', 'type', 'mime'], 'required'],
            [['itemId', 'size', 'date_upload', 'type_file_id'], 'default', 'value' => null],
            [['sort'], 'default', 'value' => 1],
            [['itemId', 'size', 'date_upload', 'sort', 'type_file_id'], 'integer'],
            [['is_main', 'moderated'], 'boolean'],
            [['name', 'model', 'hash', 'type', 'mime'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'model' => Yii::t('app', 'Model'),
            'itemId' => Yii::t('app', 'Item ID'),
            'hash' => Yii::t('app', 'Hash'),
            'size' => Yii::t('app', 'Size'),
            'type' => Yii::t('app', 'Type'),
            'mime' => Yii::t('app', 'Mime'),
            'is_main' => Yii::t('app', 'Is Main'),
            'date_upload' => Yii::t('app', 'Date Upload'),
            'sort' => Yii::t('app', 'Sort'),
            'type_file_id' => Yii::t('app', 'ID типа файла для Фильтра'),
            'moderated' => Yii::t('app', 'File is moderated'),
        ];
    }

    public function getUrl()
    {
        return Url::to(['/file/file/download', 'id' => $this->id]);
    }

    public function getWebUrl()
    {
        return str_replace('@webroot', '', Yii::$app->modules['file']->storePath . "/" . \Yii::$app->modules['file']->getSubDirs($this->hash) . DIRECTORY_SEPARATOR . $this->hash . '.' . $this->type);
    }

    public function getPath()
    {
        return $this->getModule()->getFilesDirPath($this->hash) . DIRECTORY_SEPARATOR . $this->hash . '.' . $this->type;
    }

    /**
     * {@inheritdoc}
     * @return \app\models\FileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FileQuery(get_called_class());
    }
}
